package uk.co.borismorris.hitron

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.getAllScenarios
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.common.ResourceArg
import io.quarkus.test.junit.QuarkusIntegrationTest
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.groups.Tuple.tuple
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.Test
import uk.co.borismorris.hitron.WiremockResource.Companion.DEBUG
import uk.co.borismorris.hitron.WiremockResource.Companion.PORT
import uk.co.borismorris.hitron.WiremockResource.Companion.RESOURCE_ROOT
import java.math.BigDecimal
import java.util.concurrent.TimeUnit.MINUTES
import java.util.concurrent.TimeUnit.SECONDS

@QuarkusIntegrationTest
@QuarkusTestResource(
    value = WiremockResource::class,
    initArgs = [
        ResourceArg(name = RESOURCE_ROOT, value = "uk/co/borismorris/hitron/web/wiremock"),
        ResourceArg(name = PORT, value = "8082"),
        ResourceArg(name = DEBUG, value = "true"),
    ],
)
class NativeHitronMonitorIntegrationTest {

    @Test
    fun `Application starts successfully`() {
        await().pollInSameThread().atMost(5, MINUTES).pollInterval(1, SECONDS).untilAsserted {
            assertThat(getAllScenarios())
                .extracting("name", "state")
                .contains(tuple("Ping test", "Ping test done"))
        }
        await().pollInSameThread().atMost(1, MINUTES).pollInterval(1, SECONDS).untilAsserted {
            verify(exactly(1), postRequestedFor(urlPathEqualTo("/goform/logout")))
        }

        verify(exactly(2), postRequestedFor(urlPathEqualTo("/goform/login")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/getSysInfo.asp")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/vmb_public_info.asp")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/vmb_retry_times.asp")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/vmb_service_info.asp")))
        verify(exactly(1), postRequestedFor(urlPathEqualTo("/goform/TestDiag")))
        verify(exactly(3), getRequestedFor(urlPathEqualTo("/data/getAdminDiag.asp")))
        verify(exactly(0), postRequestedFor(urlPathEqualTo("/goform/Reboot")))

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/influx/api/v2/write")))

        assertMetricsPublishedFor("8.8.8.8", BigDecimal.ZERO)
    }
}
