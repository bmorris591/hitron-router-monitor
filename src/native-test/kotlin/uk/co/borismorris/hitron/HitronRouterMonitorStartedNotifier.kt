package uk.co.borismorris.hitron

import io.quarkus.test.common.IntegrationTestStartedNotifier

class HitronRouterMonitorStartedNotifier : IntegrationTestStartedNotifier {
    override fun check(context: IntegrationTestStartedNotifier.Context?) = object : IntegrationTestStartedNotifier.Result {
        override fun isStarted() = true
        override fun isSsl() = false
    }
}
