package uk.co.borismorris.hitron.web

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import uk.co.borismorris.hitron.web.TestType.PING
import java.time.Duration

internal class DiagnosticTestParserKtTest {

    @ParameterizedTest
    @ValueSource(strings = ["PING 8.8.8.8 (8.8.8.8) from 62.30.200.104: 56 data bytes"])
    fun `I can parse a header row`(row: String) {
        val headerRow = row.parseHeaderRow()
        assertThat(headerRow).isNotNull()
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "64 bytes from 8.8.8.8: seq=0 ttl=117 time=330.000 ms",
            "64 bytes from 8.8.8.8: seq=1 ttl=117 time=100.000 ms",
            "64 bytes from 8.8.8.8: seq=2 ttl=117 time=150.000 ms",
            "64 bytes from 8.8.8.8: seq=3 ttl=117 time=170.000 ms",
        ],
    )
    fun `I can parse a ping row`(row: String) {
        val pingRow = row.parsePingRow()
        assertThat(pingRow).isNotNull()
    }

    @ParameterizedTest
    @ValueSource(strings = ["4 packets transmitted, 4 packets received, 0% packet loss"])
    fun `I can parse a packet statistics row`(row: String) {
        val packetStaticsRow = row.parsePacketStatisticsRow()
        assertThat(packetStaticsRow).isNotNull()
    }

    @ParameterizedTest
    @ValueSource(strings = ["round-trip min/avg/max = 100.000/187.500/330.000 ms"])
    fun `I can parse a round trip statistics row`(row: String) {
        val roundTripStatistics = row.parseRoundTripStatisticsRow()
        assertThat(roundTripStatistics).isNotNull()
            .extracting("min", "average", "max")
            .containsExactly(Duration.ofMillis(100), Duration.ofNanos(187_500_000), Duration.ofMillis(330))
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            """
            |PING 8.8.8.8 (8.8.8.8) from 62.30.200.104: 56 data bytes#
            |64 bytes from 8.8.8.8: seq=0 ttl=117 time=330.000 ms#
            |64 bytes from 8.8.8.8: seq=1 ttl=117 time=100.000 ms#
            |64 bytes from 8.8.8.8: seq=2 ttl=117 time=150.000 ms#
            |64 bytes from 8.8.8.8: seq=3 ttl=117 time=170.000 ms#
            |#
            |--- 8.8.8.8 ping statistics ---#
            |4 packets transmitted, 4 packets received, 0% packet loss#
            |round-trip min/avg/max = 100.000/187.500/330.000 ms#""",
            """
            |PING 8.8.8.8 (8.8.8.8) from 62.30.200.104: 56 data bytes#
            |#
            |--- 8.8.8.8 ping statistics ---#
            |4 packets transmitted, 0 packets received, 100% packet loss#
            """,
        ],
    )
    fun `I can parse ping test results`(results: String) {
        val diagnosticTestStatus = DiagnosticTestStatus("IPv4", results.trimMargin().replace("\n", ""), 1)
        val diagnosticTestResult = diagnosticTestStatus.parseTestResult(PING)
        assertThat(diagnosticTestResult).isNotNull().isInstanceOfSatisfying(PingTestResult::class.java) {
            assertThat(it.headerRow).isNotNull()
            assertThat(it.packetStatistics).isNotNull()
        }
    }
}
