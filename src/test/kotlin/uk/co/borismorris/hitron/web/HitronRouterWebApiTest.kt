package uk.co.borismorris.hitron.web

import com.github.tomakehurst.wiremock.client.WireMock.getAllScenarios
import com.github.tomakehurst.wiremock.client.WireMock.resetAllScenarios
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.QuarkusTestProfile
import io.quarkus.test.junit.QuarkusTestProfile.TestResourceEntry
import io.quarkus.test.junit.TestProfile
import jakarta.inject.Inject
import jakarta.ws.rs.core.Response
import org.apache.http.client.CookieStore
import org.apache.http.impl.cookie.BasicClientCookie
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.groups.Tuple.tuple
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import uk.co.borismorris.hitron.WiremockResource
import uk.co.borismorris.hitron.WiremockResource.Companion.DEBUG
import uk.co.borismorris.hitron.WiremockResource.Companion.RESOURCE_ROOT
import java.util.function.Consumer

const val SESSION_COOKIE_NAME = "sessionindex"
const val SESSION_ID = "0&userid=Mk6B4ltwfG9n7lZxTQAuZnDu2JYzlu66"
private val SESSION_COOKIE = BasicClientCookie(SESSION_COOKIE_NAME, SESSION_ID).apply {
    domain = "localhost"
}

@QuarkusTest
@TestProfile(ApiTestProfile::class)
internal class HitronRouterWebApiTest {

    @Inject
    @RestClient
    lateinit var api: HitronRouterWebApi

    @Inject
    lateinit var cookieStore: CookieStore

    @AfterEach
    fun teardown() {
        resetAllScenarios()
    }

    @Test
    fun `When I log in without a pression then I receive the presession cookie`() {
        api.login("username", "password").use { response ->
            assertThat(response.status).isEqualTo(302)
            assertThat(response.cookies).isNotEmpty
            val preSession = response.cookies["preSession"]
            assertThat(preSession).isNotNull()
            assertThat(preSession!!.value).isEqualTo("1TPu6KC235ylGi0CtXbpyckaXgBQ8fAl")
        }
    }

    @Test
    fun `When I log in with a presession then I receive a session cookie`() {
        api.login("username", "password", preSession = "1TPu6KC235ylGi0CtXbpyckaXgBQ8fAl").use { response ->
            assertThat(response.status).isEqualTo(200)
            assertThat(response.cookies).isNotEmpty
            assertThat(response.readEntity(String::class.java)).isEqualTo("success")
            val session = response.cookies[SESSION_COOKIE_NAME]
            assertThat(session).isNotNull()
            assertThat(session!!.value).isEqualTo(SESSION_ID)
        }
        assertThat(cookieStore.cookies)
            .extracting("name", "value")
            .contains(tuple(SESSION_COOKIE_NAME, SESSION_ID))
    }

    @Test
    fun `Given I have logged in I can log out with the session`() {
        cookieStore.addCookie(SESSION_COOKIE)

        api.logout().use { response ->
            assertThat(response.status).isEqualTo(302)
        }
    }

    @Test
    fun `Given I have logged in I can query system information`() {
        cookieStore.addCookie(SESSION_COOKIE)

        val response = api.systemInformation()

        assertThat(response).singleElement().satisfies(
            Consumer {
                assertThat(it.hardwareVersion).isEqualTo("2D")
            },
        )
    }

    @Test
    fun `Given I have logged in I can query account information`() {
        cookieStore.addCookie(SESSION_COOKIE)

        val response = api.virginMediaBusinessAccountInformation()

        assertThat(response).singleElement().satisfies(
            Consumer {
                assertThat(it.username).isEqualTo("ab12345678@cable.vmbusiness.net")
                assertThat(it.password).isEqualTo("mPa6CrBK")
            },
        )
    }

    @Test
    fun `Given I have logged in I can query public information`() {
        cookieStore.addCookie(SESSION_COOKIE)

        val response = api.virginTunnelPublicInformation()

        assertThat(response).singleElement().satisfies(
            Consumer {
                assertThat(it.gatewayIp).isEqualTo("127.0.0.1")
                assertThat(it.status).isEqualTo("ON")
                assertThat(it.cidrPrefix).isEqualTo("32")
            },
        )
    }

    @Test
    fun `Given I have logged in I can query retry information`() {
        cookieStore.addCookie(SESSION_COOKIE)

        val response = api.virginTunnelRetries()

        assertThat(response).singleElement().satisfies(
            Consumer {
                assertThat(it.retryTimes).isEqualTo(10)
            },
        )
    }

    @Test
    fun `Given I have logged in I can query service information`() {
        cookieStore.addCookie(SESSION_COOKIE)

        val response = api.virginTunnelServiceInformation()

        assertThat(response).singleElement().satisfies(
            Consumer {
                assertThat(it.wanIp).isEqualTo("127.0.0.1/32")
                assertThat(it.radius).isEqualTo("Accounting Start")
                assertThat(it.greTunnelStatus).isEqualTo("PING Success")
            },
        )
    }

    @Test
    fun `Given I have logged in I can get a csrf token`() {
        cookieStore.addCookie(SESSION_COOKIE)

        val response = api.csrfToken()

        assertThat(response.token).isEqualTo("9QeGKwhLrrupRSgPYlKw967Nh872XFE6")
    }

    @Test
    fun `Given I have logged in I can start a ping test`() {
        cookieStore.addCookie(SESSION_COOKIE)

        val diagnosticTest = DiagnosticTest("IPv4", 1, 1, "", "8.8.8.8")
        api.diagnosticTest(diagnosticTest).use { response ->
            assertThat(response).isNotNull()
            assertThat(response.statusInfo.family).isEqualTo(Response.Status.Family.SUCCESSFUL)
        }
    }

    @Test
    fun `Given I have logged in I can query test status`() {
        cookieStore.addCookie(SESSION_COOKIE)

        val diagnosticTestStatus = api.diagnosticTestStatus()

        assertThat(diagnosticTestStatus).singleElement().satisfies(
            Consumer {
                assertThat(it.testStatus).isEqualTo(1)
                assertThat(it.testType).isEqualTo("IPv4")
                assertThat(it.testResult).contains("PING 8.8.8.8").hasSize(412)
            },
        )
    }

    @Test
    fun `Given I have logged I can run a status test`() {
        cookieStore.addCookie(SESSION_COOKIE)

        val diagnosticTest = DiagnosticTest("IPv4", 1, 1, "", "8.8.8.8")
        api.diagnosticTest(diagnosticTest).use { response ->
            assertThat(response).isNotNull()
            assertThat(response.statusInfo.family).isEqualTo(Response.Status.Family.SUCCESSFUL)
        }

        assertThat(getAllScenarios()).extracting("name", "state").contains(tuple("Ping test", "Ping test started"))

        val diagnosticTestStatus = api.diagnosticTestStatus()

        assertThat(diagnosticTestStatus).singleElement().satisfies(
            Consumer {
                assertThat(it.testStatus).isEqualTo(0)
                assertThat(it.testType).isEqualTo("IPv4")
                assertThat(it.testResult).contains("PING 8.8.8.8").hasSize(110)
            },
        )

        assertThat(getAllScenarios()).extracting("name", "state").contains(tuple("Ping test", "First ping done"))
    }

    @Test
    fun `Given I have logged in I reboot the modem`() {
        cookieStore.addCookie(SESSION_COOKIE)

        api.reboot().use { response ->
            assertThat(response).isNotNull()
        }
    }
}

class ApiTestProfile : QuarkusTestProfile {
    override fun getConfigOverrides() = mutableMapOf(
        "hitron.username" to "username",
        "hitron.password" to "password",
        "quarkus.scheduler.enabled" to "false",
    )

    override fun testResources() = mutableListOf(
        TestResourceEntry(
            WiremockResource::class.java,
            mutableMapOf(RESOURCE_ROOT to "/uk/co/borismorris/hitron/web/wiremock", DEBUG to "true"),
        ),
    )
}
