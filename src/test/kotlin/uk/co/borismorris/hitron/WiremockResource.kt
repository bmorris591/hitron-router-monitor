package uk.co.borismorris.hitron

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.configureFor
import com.github.tomakehurst.wiremock.common.Slf4jNotifier
import com.github.tomakehurst.wiremock.core.Options
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.jvm.javaField

class WiremockResource : QuarkusTestResourceLifecycleManager {
    companion object {
        const val RESOURCE_ROOT = "wiremock.resource-root"
        const val PORT = "wiremock.port"
        const val DEBUG = "wiremock.debug"
    }

    lateinit var options: WireMockConfiguration
    lateinit var server: WireMockServer

    override fun init(initArgs: MutableMap<String, String>) {
        val options = WireMockConfiguration.wireMockConfig()
            .jettyAcceptors(2)
            .jettyAcceptQueueSize(1024)
            .asynchronousResponseEnabled(false)
            .extensions(ResponseTemplateTransformer(false), FormDataMatcher())

        initArgs[RESOURCE_ROOT]?.let { options.usingFilesUnderClasspath(it) }
        (initArgs[PORT]?.toIntOrNull() ?: Options.DYNAMIC_PORT).also { options.port(it) }
        options.notifier(Slf4jNotifier(initArgs[DEBUG]?.toBoolean() ?: false))

        this.options = options
    }

    override fun start(): MutableMap<String, String> {
        server = WireMockServer(options)
        server.start()

        return mutableMapOf(
            "hitron-router-api/mp-rest/url" to "${server.baseUrl()}/",
            "influx.url" to "${server.baseUrl()}/influx",
        )
    }

    override fun inject(testInstance: Any?) {
        requireNotNull(testInstance)

        configureFor(server.port())

        testInstance::class.declaredMemberProperties
            .filter { it.javaField?.isAnnotationPresent(InjectServer::class.java) ?: false }
            .map { it as KMutableProperty<*> }
            .forEach { it.setter.call(testInstance, server) }
    }

    override fun stop() {
        if (this::server.isInitialized) {
            server.stop()
        }
    }
}

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class InjectServer
