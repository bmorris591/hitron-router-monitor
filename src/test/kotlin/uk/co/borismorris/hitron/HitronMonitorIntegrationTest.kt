package uk.co.borismorris.hitron

import com.github.tomakehurst.wiremock.admin.model.ServeEventQuery
import com.github.tomakehurst.wiremock.client.WireMock.and
import com.github.tomakehurst.wiremock.client.WireMock.equalTo
import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.findStubsByMetadata
import com.github.tomakehurst.wiremock.client.WireMock.getAllScenarios
import com.github.tomakehurst.wiremock.client.WireMock.getAllServeEvents
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.matchingJsonPath
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.QuarkusTestProfile
import io.quarkus.test.junit.QuarkusTestProfile.TestResourceEntry
import io.quarkus.test.junit.TestProfile
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.groups.Tuple
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.Test
import uk.co.borismorris.hitron.WiremockResource.Companion.DEBUG
import uk.co.borismorris.hitron.WiremockResource.Companion.RESOURCE_ROOT
import java.math.BigDecimal
import java.util.concurrent.TimeUnit.MINUTES
import java.util.concurrent.TimeUnit.SECONDS
import java.util.function.Consumer

@QuarkusTest
@TestProfile(SuccessfulTestProfile::class)
class SuccessfulHitronMonitorIntegrationTest {

    @Test
    fun `Application starts successfully`() {
        await().pollInSameThread().atMost(1, MINUTES).pollInterval(1, SECONDS).untilAsserted {
            assertThat(getAllScenarios())
                .extracting("name", "state")
                .contains(Tuple.tuple("Ping test", "Ping test done"))
        }
        await().pollInSameThread().atMost(1, MINUTES).pollInterval(1, SECONDS).untilAsserted {
            verify(exactly(1), postRequestedFor(urlPathEqualTo("/goform/logout")))
        }

        verify(exactly(2), postRequestedFor(urlPathEqualTo("/goform/login")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/getSysInfo.asp")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/vmb_public_info.asp")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/vmb_retry_times.asp")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/vmb_service_info.asp")))
        verify(exactly(1), postRequestedFor(urlPathEqualTo("/goform/TestDiag")))
        verify(exactly(3), getRequestedFor(urlPathEqualTo("/data/getAdminDiag.asp")))
        verify(exactly(0), postRequestedFor(urlPathEqualTo("/goform/Reboot")))

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/influx/api/v2/write")))

        assertMetricsPublishedFor("8.8.8.8", BigDecimal.ZERO)
    }
}

@QuarkusTest
@TestProfile(FailedTestProfile::class)
class FailedHitronMonitorIntegrationTest {

    @Test
    fun `Application triggers restart when ping fails`() {
        await().pollInSameThread().atMost(1, MINUTES).pollInterval(1, SECONDS).untilAsserted {
            assertThat(getAllScenarios())
                .extracting("name", "state")
                .contains(Tuple.tuple("Ping test", "Ping test done"))
        }
        await().pollInSameThread().atMost(1, MINUTES).pollInterval(1, SECONDS).untilAsserted {
            verify(exactly(1), postRequestedFor(urlPathEqualTo("/goform/logout")))
        }

        verify(exactly(2), postRequestedFor(urlPathEqualTo("/goform/login")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/getSysInfo.asp")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/vmb_public_info.asp")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/vmb_retry_times.asp")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/vmb_service_info.asp")))
        verify(exactly(1), postRequestedFor(urlPathEqualTo("/goform/TestDiag")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/data/getAdminDiag.asp")))
        verify(exactly(1), postRequestedFor(urlPathEqualTo("/goform/Reboot")))

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/influx/api/v2/write")))

        assertMetricsPublishedFor("1.1.1.1", BigDecimal(100))
    }
}

fun assertMetricsPublishedFor(ip: String, packetLoss: BigDecimal) {
    val pl = packetLoss.setScale(1).toPlainString()
    val stubs = findStubsByMetadata(
        and(
            matchingJsonPath("$.destination", equalTo(ip)),
            matchingJsonPath("$.packetLoss", equalTo(pl)),
        ),
    )
    assertThat(stubs).hasSize(1)

    val events = getAllServeEvents(ServeEventQuery.forStubMapping(stubs.first().id))
    assertThat(events).singleElement().satisfies(
        Consumer {
            assertThat(it.request.bodyAsString)
                .contains("destination=$ip")
                .contains("packet_loss=$pl")
        },
    )
}

sealed class IntegrationTestProfile(pingAddress: String) : QuarkusTestProfile {

    private val args = mapOf(
        RESOURCE_ROOT to "/uk/co/borismorris/hitron/web/wiremock",
        DEBUG to "true",
    )

    private val configOverrides = mapOf(
        "hitron.username" to "username",
        "hitron.password" to "password",
        "hitron.health.ping-addresses" to pingAddress,
        "hitron.health.test-failure-threshold" to "1",
        "hitron.health.period" to "PT1H",
        "influx.auth-token" to "authauth123",
    )

    override fun getConfigOverrides() = configOverrides
    override fun testResources() = listOf(TestResourceEntry(WiremockResource::class.java, args))
}

class SuccessfulTestProfile : IntegrationTestProfile("8.8.8.8")
class FailedTestProfile : IntegrationTestProfile("1.1.1.1")
