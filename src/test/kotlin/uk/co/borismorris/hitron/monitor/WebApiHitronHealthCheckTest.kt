package uk.co.borismorris.hitron.monitor

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.QuarkusTestProfile
import io.quarkus.test.junit.TestProfile
import jakarta.inject.Inject
import org.assertj.core.api.Assertions.`as`
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.InstanceOfAssertFactories
import org.assertj.core.api.ObjectAssert
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.MockMakers
import org.mockito.MockitoAnnotations.openMocks
import org.mockito.kotlin.whenever
import uk.co.borismorris.hitron.web.HitronRouterWebSession
import uk.co.borismorris.hitron.web.PacketStatistics
import uk.co.borismorris.hitron.web.PingTestResult
import uk.co.borismorris.hitron.web.SystemInformation
import uk.co.borismorris.hitron.web.VirginMediaConnectionStatus
import uk.co.borismorris.hitron.web.VirginTunnelPublicInformation
import uk.co.borismorris.hitron.web.VirginTunnelServiceInformation
import java.math.BigDecimal
import java.net.InetAddress
import java.util.function.Consumer

val packetLossThreshold = BigDecimal("25")
val quadEight = InetAddress.getByName("8.8.8.8")
val pingAddresses = listOf(quadEight)

@QuarkusTest
@TestProfile(HealthCheckTestProfile::class)
internal class WebApiHitronHealthCheckTest {

    @Mock(mockMaker = MockMakers.INLINE)
    lateinit var routerSession: HitronRouterWebSession

    @Mock(mockMaker = MockMakers.INLINE)
    lateinit var systemInformation: SystemInformation

    @Mock(mockMaker = MockMakers.INLINE)
    lateinit var connectionStatus: VirginMediaConnectionStatus

    @Mock(mockMaker = MockMakers.INLINE)
    lateinit var tunnelPublicInformation: VirginTunnelPublicInformation

    @Mock(mockMaker = MockMakers.INLINE)
    lateinit var tunnelServiceInformation: VirginTunnelServiceInformation

    @Mock(mockMaker = MockMakers.INLINE)
    lateinit var pingTestRest: PingTestResult

    @Mock(mockMaker = MockMakers.INLINE)
    lateinit var packetStatistics: PacketStatistics

    @Inject
    lateinit var healthCheck: WebApiHitronHealthCheck

    lateinit var mockito: AutoCloseable

    @BeforeEach
    fun setup() {
        mockito = openMocks(this)

        whenever(routerSession.systemInformation()).thenReturn(systemInformation)
        whenever(routerSession.virginMediaConnectionStatus()).thenReturn(connectionStatus)
        whenever(routerSession.ping(quadEight.hostAddress)).thenReturn(pingTestRest)

        whenever(connectionStatus.tunnelPublicInformation).thenReturn(tunnelPublicInformation)
        whenever(connectionStatus.tunnelServiceInformation).thenReturn(tunnelServiceInformation)

        whenever(pingTestRest.packetStatistics).thenReturn(packetStatistics)
    }

    @AfterEach
    fun teardown() {
        mockito.close()
    }

    @Test
    fun `When there is no WAN IP then state is unhealthy`() {
        whenever(systemInformation.wanIp).thenReturn("none")
        whenever(packetStatistics.packetLoss).thenReturn(BigDecimal.ZERO)

        val healthStatus = healthCheck.checkHealth(routerSession)
        assertThat(healthStatus).isUnhealthy()
        assertThat(healthStatus).hasHealthStatusSatisfying {
            assertThat(it).isInstanceOfSatisfying(WanIpMissing::class.java) { it.isUnhealthy() }
        }
    }

    @Test
    fun `When the GRE tunnel is not ON then the state is unhealthy`() {
        whenever(systemInformation.wanIp).thenReturn("127.0.0.1")
        whenever(tunnelPublicInformation.status).thenReturn("OFF")
        whenever(packetStatistics.packetLoss).thenReturn(BigDecimal.ZERO)

        val healthStatus = healthCheck.checkHealth(routerSession)
        assertThat(healthStatus).isUnhealthy()
        assertThat(healthStatus).hasHealthStatusSatisfying {
            assertThat(it).isInstanceOfSatisfying(GreTunnelDown::class.java) { it.isUnhealthy() }
        }
    }

    @Test
    fun `When the GRE tunnel ping fails then the state is unhealthy`() {
        whenever(systemInformation.wanIp).thenReturn("127.0.0.1")
        whenever(tunnelPublicInformation.status).thenReturn("ON")
        whenever(tunnelServiceInformation.greTunnelStatus).thenReturn("PING Failed")
        whenever(packetStatistics.packetLoss).thenReturn(BigDecimal.ZERO)

        val healthStatus = healthCheck.checkHealth(routerSession)
        assertThat(healthStatus).isUnhealthy()
        assertThat(healthStatus).hasHealthStatusSatisfying {
            assertThat(it).isInstanceOfSatisfying(GreTunnelPingFailed::class.java) { it.isUnhealthy() }
        }
    }

    @Test
    fun `When the ping fails then the state is unhealthy`() {
        whenever(systemInformation.wanIp).thenReturn("127.0.0.1")
        whenever(tunnelPublicInformation.status).thenReturn("ON")
        whenever(tunnelServiceInformation.greTunnelStatus).thenReturn("PING Success")
        whenever(packetStatistics.packetLoss).thenReturn(BigDecimal("100"))

        val healthStatus = healthCheck.checkHealth(routerSession)
        assertThat(healthStatus).isUnhealthy()
        assertThat(healthStatus).hasHealthStatusSatisfying {
            assertThat(it).isInstanceOfSatisfying(PingFailed::class.java) { it.isUnhealthy() }
        }
    }

    private fun ObjectAssert<CompositeHeathStatus>.hasHealthStatusSatisfying(requirements: (HealthStatus) -> Unit) {
        satisfies(
            Consumer { status ->
                assertThat(status.asSequence().toList()).anySatisfy(
                    Consumer {
                        requirements(it)
                    },
                )
            },
        )
    }

    private fun <T : HealthStatus> ObjectAssert<T>.isUnhealthy() =
        extracting("healthy", `as`(InstanceOfAssertFactories.BOOLEAN))
            .`as`("Router is unhealthy")
            .isFalse

    @Test
    fun `When router is healthy then state returned is healthy`() {
        whenever(systemInformation.wanIp).thenReturn("127.0.0.1")
        whenever(tunnelPublicInformation.status).thenReturn("ON")
        whenever(tunnelServiceInformation.greTunnelStatus).thenReturn("PING Success")
        whenever(packetStatistics.packetLoss).thenReturn(BigDecimal.ZERO)

        val healthStatus = healthCheck.checkHealth(routerSession)
        assertThat(healthStatus)
            .extracting("healthy", `as`(InstanceOfAssertFactories.BOOLEAN))
            .`as`("Router is healthy")
            .isTrue()
    }
}

class HealthCheckTestProfile : QuarkusTestProfile {
    override fun getConfigOverrides() = mutableMapOf(
        "quarkus.scheduler.enabled" to "false",
        "hitron.username" to "username",
        "hitron.password" to "password",
        "hitron.health.ping-addresses" to pingAddresses.map { it.hostAddress }.joinToString(","),
        "hitron.health.packet-loss-threshold" to packetLossThreshold.toPlainString(),
    )
}
