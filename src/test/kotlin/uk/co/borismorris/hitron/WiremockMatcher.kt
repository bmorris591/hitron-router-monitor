package uk.co.borismorris.hitron

import com.github.tomakehurst.wiremock.extension.Parameters
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.matching.EqualToPattern
import com.github.tomakehurst.wiremock.matching.MatchResult
import com.github.tomakehurst.wiremock.matching.MatchResult.aggregate
import com.github.tomakehurst.wiremock.matching.MatchResult.noMatch
import com.github.tomakehurst.wiremock.matching.RequestMatcherExtension
import java.net.URLDecoder
import java.nio.charset.StandardCharsets

const val DATA = "data"

class FormDataMatcher : RequestMatcherExtension() {

    @Suppress("UNCHECKED_CAST")
    override fun match(request: Request?, parameters: Parameters?): MatchResult {
        requireNotNull(request)
        requireNotNull(parameters)

        val body = URLDecoder.decode(request.bodyAsString, StandardCharsets.US_ASCII)

        val formData = body.splitToSequence('&')
            .map {
                val split = it.split('=', limit = 2)
                if (split.size == 1) {
                    split.first() to "true"
                } else {
                    split.first() to split[1]
                }
            }
            .groupBy({ it.first }, { it.second })
            .toMap()

        val expected = parameters[DATA] as Map<String, String>

        if (formData.size != expected.size) {
            return noMatch()
        }

        val matches = expected.map { (k, v) ->
            val actualValue = formData[k]
            when {
                actualValue == null -> noMatch()
                actualValue.size != 1 -> noMatch()
                else -> EqualToPattern(v, false).match(actualValue.first())
            }
        }.toList()
        return aggregate(matches)
    }

    override fun getName() = "form-data"
}
