package uk.co.borismorris.hitron

import io.micrometer.core.instrument.Tag
import io.micrometer.core.instrument.config.MeterFilter
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.inject.Produces
import jakarta.inject.Singleton

@ApplicationScoped
class MetricsConfiguration {

    @Produces
    @Singleton
    fun configureAllRegistries(): MeterFilter = MeterFilter.commonTags(listOf(Tag.of("application", "hitron-router-monitor")))

    @Produces
    @Singleton
    fun disablePoolMetric(): MeterFilter = MeterFilter.denyNameStartsWith("worker.pool.ratio")
}
