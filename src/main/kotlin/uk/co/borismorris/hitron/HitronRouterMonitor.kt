package uk.co.borismorris.hitron

import io.opentelemetry.instrumentation.annotations.WithSpan
import io.quarkus.logging.Log
import io.quarkus.runtime.Quarkus
import io.quarkus.runtime.QuarkusApplication
import io.quarkus.runtime.StartupEvent
import io.quarkus.runtime.annotations.QuarkusMain
import io.quarkus.scheduler.Scheduled
import io.quarkus.scheduler.Scheduled.ConcurrentExecution.SKIP
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.event.Observes
import jakarta.enterprise.inject.Instance
import uk.co.borismorris.hitron.conf.HealthCheckConfig
import uk.co.borismorris.hitron.conf.HitronRouterConfig
import uk.co.borismorris.hitron.monitor.HealthActionHandler
import uk.co.borismorris.hitron.monitor.HitronHealthCheck
import uk.co.borismorris.hitron.monitor.HitronHealthHandler
import uk.co.borismorris.hitron.web.HitronRouterApi

@QuarkusMain
class HitronRouterMonitor : QuarkusApplication {

    override fun run(vararg args: String?): Int {
        Quarkus.waitForExit()
        return 0
    }
}

@ApplicationScoped
class HitronRouterManager(
    private val hitronRouterApi: HitronRouterApi,
    private val hitronHealthCheck: HitronHealthCheck,
    private val healthHandlers: Instance<HitronHealthHandler>,
    private val actionHandlers: Instance<HealthActionHandler>,
) {

    @WithSpan
    @Suppress("NestedBlockDepth", "TooGenericExceptionCaught")
    @Scheduled(identity = "hitronRouterConnectivityChecker", every = "{hitron.health.period}", concurrentExecution = SKIP)
    fun onPoll() {
        try {
            hitronRouterApi.login().use { session ->
                val health = hitronHealthCheck.checkHealth(session)
                if (health.healthy) {
                    Log.infov("Internet is healthy. {0}", health)
                } else {
                    Log.errorv("Internet is unhealthy. {0}", health)
                }

                val actions = healthHandlers.asSequence()
                    .sortedBy { it.priority }
                    .flatMap { it.handle(health, session) }
                    .toSet()

                Log.infov("Actions {0}", actions)

                actions.forEach { action ->
                    actionHandlers.forEach { handler ->
                        handler.handleAction(action, session)
                    }
                }
            }
        } catch (e: Exception) {
            Log.error("Failed to check health.", e)
        }
    }
}

@ApplicationScoped
class OnStartup(
    private val routerConfig: HitronRouterConfig,
    private val healthCheckConfig: HealthCheckConfig,
) {

    fun onStart(
        @Observes
        @Suppress("UNUSED_PARAMETER")
        ev: StartupEvent?,
    ) {
        Log.infov("Hitron Router API config: {0}", routerConfig)
        Log.infov("Health check config: {0}", healthCheckConfig)
    }
}
