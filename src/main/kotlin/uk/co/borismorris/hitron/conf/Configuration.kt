package uk.co.borismorris.hitron.conf

import com.influxdb.client.domain.WritePrecision
import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithDefault
import java.math.BigDecimal
import java.net.InetAddress
import java.time.Duration
import java.util.Optional

@ConfigMapping(prefix = "hitron")
interface HitronRouterConfig {
    fun username(): String
    fun password(): String

    @WithDefault("false")
    fun dryRun(): Boolean
}

@ConfigMapping(prefix = "hitron.health")
interface HealthCheckConfig {
    fun period(): Duration
    fun pingAddresses(): List<InetAddress>

    @WithDefault("50")
    fun packetLossThreshold(): BigDecimal

    @WithDefault("2")
    fun testFailureThreshold(): Int

    @WithDefault("PT1H")
    fun minTimeBetweenReboots(): Duration
}

@ConfigMapping(prefix = "influx")
interface InfluxConfig {
    fun url(): String
    fun bucket(): String
    fun organization(): String
    fun authToken(): Optional<String>

    @WithDefault("MS")
    fun precision(): WritePrecision
}
