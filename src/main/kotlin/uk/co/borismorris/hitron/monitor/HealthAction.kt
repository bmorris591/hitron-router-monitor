package uk.co.borismorris.hitron.monitor

import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.Point
import io.quarkus.logging.Log
import jakarta.enterprise.context.ApplicationScoped
import uk.co.borismorris.hitron.conf.HealthCheckConfig
import uk.co.borismorris.hitron.influx.InfluxClient
import uk.co.borismorris.hitron.web.HitronRouterSession
import uk.co.borismorris.hitron.web.PingTestResult
import java.time.Duration
import java.time.Instant
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference

sealed interface HealthAction

object HealthyAction : HealthAction {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

    override fun toString(): String {
        return "HealthyAction()"
    }
}

object RestartAction : HealthAction {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

    override fun toString(): String {
        return "RestartAction()"
    }
}

@JvmRecord
data class RecordMetricsAction<T : HealthStatus>(val status: T) : HealthAction

interface HealthActionHandler {
    fun handleAction(action: HealthAction, session: HitronRouterSession)
}

@ApplicationScoped
class RestartActionHandler(private val config: HealthCheckConfig) : HealthActionHandler {
    private val failureCount = AtomicInteger(0)
    private val restartTriggerTime = AtomicReference(Instant.MIN)

    override fun handleAction(action: HealthAction, session: HitronRouterSession) {
        when (action) {
            is RestartAction -> restart(session)
            is HealthyAction -> reset()
            else -> Unit
        }
    }

    private fun restart(session: HitronRouterSession) {
        if (failureCount.incrementAndGet() >= config.testFailureThreshold()) {
            if (Duration.between(restartTriggerTime.get(), Instant.now()) > config.minTimeBetweenReboots()) {
                Log.info("Restarting router. Failure count $failureCount and threshold ${config.testFailureThreshold()}. Previous restart $restartTriggerTime and min time between ${config.minTimeBetweenReboots()}")
                session.reboot()
                restartTriggerTime.set(Instant.now())
            } else {
                Log.info("Not restarting router, too recently restarted. Failure count $failureCount and threshold ${config.testFailureThreshold()}. Previous restart $restartTriggerTime and min time between ${config.minTimeBetweenReboots()}")
            }
        } else {
            Log.info("Not restarting router, failure count not reached. Failure count $failureCount and threshold ${config.testFailureThreshold()}.")
        }
    }

    private fun reset() {
        val old = failureCount.getAndUpdate { 0 }
        if (old > 0) {
            Log.infov("Resetting failure count. Was {0}", old)
        }
    }
}

@ApplicationScoped
class RecordMetricsActionHandler(private val influx: InfluxClient) : HealthActionHandler {
    @Suppress("TooGenericExceptionCaught")
    override fun handleAction(action: HealthAction, session: HitronRouterSession) {
        when (action) {
            is RecordMetricsAction<*> -> try {
                recordMetrics(action)
            } catch (ex: Exception) {
                Log.errorv(ex, "Failed to record metrics {0}", action)
            }
            else -> Unit
        }
    }

    private fun recordMetrics(action: RecordMetricsAction<*>) {
        when (val status = action.status) {
            is HealthyPingCheck -> status.recordPingMetrics { pingTestResult }
            is PingFailed -> status.recordPingMetrics { pingTestResult }
            else -> Unit
        }
    }

    private fun <T : HealthStatus> T.recordPingMetrics(pingTestResult: T.() -> PingTestResult) {
        pingTestResult().recordPingMetrics(timestamp)
    }

    private fun PingTestResult.recordPingMetrics(timestamp: Instant) {
        fun Point.addHeaderRow() = with(headerRow) {
            addTag("source", source.hostAddress)
                .addTag("destination", destination.hostAddress)
        }

        fun Point.addPacketStats() = with(packetStatistics) {
            addField("packets_sent", packetsTransmitted)
                .addField("packets_received", packetsReceived)
                .addField("packet_loss", packetLoss)
        }

        fun Point.addRoundTripStats() = roundTripStatistics?.run {
            addField("round_trip_min", min.toNanos())
                .addField("round_trip_max", max.toNanos())
                .addField("round_trip_average", average.toNanos())
        } ?: this

        val point = Point.measurement("ping")
            .time(timestamp.toEpochMilli(), WritePrecision.MS)
            .addHeaderRow()
            .addPacketStats()
            .addRoundTripStats()

        if (point.hasFields()) {
            influx.writePoint(point)
        }
    }
}
