package uk.co.borismorris.hitron.monitor

import io.quarkus.logging.Log
import jakarta.enterprise.context.ApplicationScoped
import uk.co.borismorris.hitron.web.HitronRouterSession
import kotlin.reflect.KClass

const val DEFAULT_PRIORITY = 0

interface HitronHealthHandler {
    val priority: Int
        get() = DEFAULT_PRIORITY

    fun handle(status: HealthStatus, session: HitronRouterSession): Sequence<HealthAction>
}

abstract class TypesafeHealthHandler<T : Any>(private val type: KClass<T>) : HitronHealthHandler {
    private fun canHandle(unhealthy: HealthStatus) = type.isInstance(unhealthy)

    override fun handle(status: HealthStatus, session: HitronRouterSession) = status.asSequence()
        .flatMap {
            if (!canHandle(it)) {
                emptySequence()
            } else {
                @Suppress("UNCHECKED_CAST")
                handleTypesafe(it as T, session)
            }
        }

    internal abstract fun handleTypesafe(status: T, session: HitronRouterSession): Sequence<HealthAction>
}

@ApplicationScoped
class HealthyHandler : HitronHealthHandler {
    override fun handle(status: HealthStatus, session: HitronRouterSession) = if (status.healthy) {
        sequenceOf(HealthyAction)
    } else {
        emptySequence()
    }
}

@ApplicationScoped
class WanIpMissingHandler : TypesafeHealthHandler<WanIpMissing>(WanIpMissing::class) {
    override fun handleTypesafe(status: WanIpMissing, session: HitronRouterSession): Sequence<HealthAction> {
        Log.warnv("WAN IP missing {0}", status)
        return emptySequence()
    }
}

@ApplicationScoped
class GreTunnelFailureHandler : TypesafeHealthHandler<GreTunnelFailure>(GreTunnelFailure::class) {
    override fun handleTypesafe(status: GreTunnelFailure, session: HitronRouterSession): Sequence<HealthAction> {
        Log.warnv("GRE tunnel down missing {0}", status)
        return when (status) {
            is GreTunnelPingFailed -> sequenceOf(RestartAction)
            else -> emptySequence()
        }
    }
}

@ApplicationScoped
class PingFailureHandler : TypesafeHealthHandler<PingFailed>(PingFailed::class) {
    override fun handleTypesafe(status: PingFailed, session: HitronRouterSession): Sequence<HealthAction> {
        Log.warnv("Ping failed {0}", status)
        return sequenceOf(RestartAction)
    }
}

@ApplicationScoped
class MetricRecordingHandler : HitronHealthHandler {
    override fun handle(status: HealthStatus, session: HitronRouterSession) = status.asSequence().map { RecordMetricsAction(it) }
}
