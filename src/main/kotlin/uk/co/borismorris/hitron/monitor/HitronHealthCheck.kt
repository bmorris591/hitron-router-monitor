package uk.co.borismorris.hitron.monitor

import io.opentelemetry.instrumentation.annotations.WithSpan
import io.quarkus.logging.Log
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.inject.Instance
import uk.co.borismorris.hitron.conf.HealthCheckConfig
import uk.co.borismorris.hitron.web.HitronRouterSession
import uk.co.borismorris.hitron.web.PingTestResult
import uk.co.borismorris.hitron.web.SystemInformation
import uk.co.borismorris.hitron.web.VirginMediaConnectionStatus
import java.net.InetAddress
import java.time.Instant

sealed interface HealthStatus : Sequence<HealthStatus> {
    val timestamp: Instant
    val healthy: Boolean

    fun isUnhealthy() = !healthy

    override fun iterator() = setOf(this).iterator()
}

sealed class Unhealthy : HealthStatus {
    override val timestamp: Instant = Instant.now()
    abstract val reason: String

    override val healthy: Boolean
        get() = false
}

sealed class Healthy : HealthStatus {
    override val timestamp: Instant = Instant.now()
    override val healthy: Boolean
        get() = true
}

data class CompositeHeathStatus(val stauses: Collection<HealthStatus>) : HealthStatus {
    override val timestamp: Instant = Instant.now()
    override val healthy = stauses.all { it.healthy }

    operator fun plus(heathStatus: CompositeHeathStatus) = CompositeHeathStatus(stauses + heathStatus.stauses)

    override fun iterator() = iterator {
        stauses.forEach { status ->
            yieldAll(status)
        }
    }
}

data class WanIpMissing(val systemInformation: SystemInformation, override val reason: String) : Unhealthy()
sealed class GreTunnelFailure : Unhealthy() {
    abstract val connectionStatus: VirginMediaConnectionStatus
}

data class GreTunnelDown(override val connectionStatus: VirginMediaConnectionStatus, override val reason: String) : GreTunnelFailure()
data class GreTunnelPingFailed(override val connectionStatus: VirginMediaConnectionStatus, override val reason: String) : GreTunnelFailure()
data class PingFailed(val pingTestResult: PingTestResult, override val reason: String) : Unhealthy()
data class HealthySystemInformation(val systemInformation: SystemInformation) : Healthy()
data class HealthyGreTunnel(val virginMediaConnectionStatus: VirginMediaConnectionStatus) : Healthy()
data class HealthyPingCheck(val pingTestResult: PingTestResult) : Healthy()
interface HitronHealthVoter {
    val priority: Int

    fun checkHealth(session: HitronRouterSession): HealthStatus
}

interface HitronHealthCheck {
    fun checkHealth(session: HitronRouterSession): HealthStatus
}

@ApplicationScoped
class WebApiHitronHealthCheck(private val healthVoters: Instance<HitronHealthVoter>) : HitronHealthCheck {
    @WithSpan
    override fun checkHealth(session: HitronRouterSession): CompositeHeathStatus {
        val stauses = healthVoters
            .asSequence()
            .sortedBy { it.priority }
            .map { it.checkHealth(session) }
            .iterator()

        val s = mutableListOf<HealthStatus>()
        while (stauses.hasNext()) {
            val status = stauses.next()
            s.add(status)
            if (status.isUnhealthy()) {
                break
            }
        }
        return CompositeHeathStatus(s.toList())
    }
}

const val NONE = "none"

@ApplicationScoped
class HasWanIpVoter : HitronHealthVoter {
    override val priority: Int
        get() = 100

    override fun checkHealth(session: HitronRouterSession) = with(session.systemInformation()) {
        Log.infov("System information {0}", this)
        if (wanIp.lowercase() == NONE) {
            WanIpMissing(this, "No WAN IP")
        } else {
            HealthySystemInformation(this)
        }
    }

    @ApplicationScoped
    class HasGreTunnelVoter : HitronHealthVoter {
        override val priority: Int
            get() = 200

        override fun checkHealth(session: HitronRouterSession): HealthStatus = with(session.virginMediaConnectionStatus()) {
            Log.infov("VMB information {0}", this)
            return if (tunnelPublicInformation.status != "ON") {
                GreTunnelDown(this, "GRE tunnel down")
            } else if (tunnelServiceInformation.greTunnelStatus != "PING Success") {
                GreTunnelPingFailed(this, "GRE tunnel ping failed")
            } else {
                HealthyGreTunnel(this)
            }
        }
    }

    @ApplicationScoped
    class PingCheckVoter(private val healthCheckConfig: HealthCheckConfig) : HitronHealthVoter {
        override val priority: Int
            get() = 300

        override fun checkHealth(session: HitronRouterSession): HealthStatus {
            val pingResults = healthCheckConfig.pingAddresses().map {
                with(session.runPingTest(it)) {
                    Log.infov("Test completed {0}.", this)
                    if (packetStatistics.packetLoss > healthCheckConfig.packetLossThreshold()) {
                        PingFailed(this, "Packet loss too high to $it")
                    } else {
                        HealthyPingCheck(this)
                    }
                }
            }.toList()
            return CompositeHeathStatus(pingResults)
        }

        private fun HitronRouterSession.runPingTest(address: InetAddress) = ping(address.hostAddress)
    }
}
