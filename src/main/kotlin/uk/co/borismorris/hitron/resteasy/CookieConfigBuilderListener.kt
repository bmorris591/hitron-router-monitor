package uk.co.borismorris.hitron.resteasy

import io.quarkus.logging.Log
import io.quarkus.runtime.annotations.RegisterForReflection
import jakarta.enterprise.inject.spi.CDI
import org.apache.http.client.CookieStore
import org.apache.http.impl.client.HttpClients
import org.eclipse.microprofile.rest.client.RestClientBuilder
import org.eclipse.microprofile.rest.client.spi.RestClientBuilderListener
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient43Engine

@RegisterForReflection
class CookieConfigBuilderListener : RestClientBuilderListener {

    override fun onNewBuilder(builder: RestClientBuilder) {
        Log.infov("New rest client builder created {0}", builder)
        val cookieStore = CDI.current().select(CookieStore::class.java).get()
        val httpClient = HttpClients.custom()
            .useSystemProperties()
            .setDefaultCookieStore(cookieStore)
            .build()
        Log.infov("Created sync HttpClient {0}", httpClient)
        val engine = ApacheHttpClient43Engine(httpClient, true)
        Log.infov("Created ApacheHttpClient43Engine {0}", engine)
        builder.property("resteasy.httpEngine", engine)
        Log.info("Set http engine onto RestClientBuilder")
    }
}
