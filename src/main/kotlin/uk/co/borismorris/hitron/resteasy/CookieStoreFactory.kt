package uk.co.borismorris.hitron.resteasy

import io.quarkus.arc.Unremovable
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.inject.Produces
import org.apache.http.client.CookieStore
import org.apache.http.impl.client.BasicCookieStore

class CookieStoreFactory {

    @Unremovable
    @Produces
    @ApplicationScoped
    fun cookieStore(): CookieStore = BasicCookieStore()
}
