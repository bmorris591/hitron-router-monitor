package uk.co.borismorris.hitron.web

import io.opentelemetry.instrumentation.annotations.SpanAttribute
import io.opentelemetry.instrumentation.annotations.WithSpan
import io.quarkus.logging.Log
import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.core.Response
import org.apache.http.HttpStatus
import org.apache.http.client.CookieStore
import org.eclipse.microprofile.rest.client.inject.RestClient
import uk.co.borismorris.hitron.conf.HitronRouterConfig
import java.time.Instant
import java.util.Date
import java.util.concurrent.TimeUnit

interface HitronRouterApi {
    fun login(): HitronRouterSession
}

interface HitronRouterSession : AutoCloseable {
    fun logout()

    fun systemInformation(): SystemInformation

    fun ping(ipAddress: String): PingTestResult

    fun traceroute(ipAddress: String): TracerouteTestResult

    fun virginMediaConnectionStatus(): VirginMediaConnectionStatus

    fun reboot()

    override fun close() {
        logout()
    }
}

enum class TestType(val testMode: Int) {
    PING(1),
    TRACEROUTE(0),
}

const val SUCCESS = "success"
const val REPEAT_LOGIN = "repeat login"

@ApplicationScoped
class HitronRouterApiWebApiImpl(
    private val cookieStore: CookieStore,
    @RestClient private val api: HitronRouterWebApi,
    private val config: HitronRouterConfig,
) : HitronRouterApi {
    override fun login(): HitronRouterWebSession {
        var preSession = cookieStore.preSession()
        if (preSession == null) {
            api.login().use {
                Log.infov("Logging in to retrieve preSession {0}", it.status)
            }
            preSession = cookieStore.preSession() ?: throw HitronRouterException("Expected to get preSession cookie from request. Cookie is missing")
        }

        Log.debugv("PreSession {0}", preSession)

        api.login(preSession).use {
            val body = it.bodyAsString()
            Log.debugv("Logged in {0}. Response {1}", it.status, body)
            if (it.status != HttpStatus.SC_OK) {
                throw HitronRouterException("Failed to log in. Status ${it.status}. Response $body")
            }
            when (body.lowercase()) {
                SUCCESS -> Log.infov("Login success {0}", body)
                REPEAT_LOGIN -> throw ConcurrentSessionException("Repeat login. Status ${it.status}. Response $body")
                else -> throw HitronRouterException("Login failed. Status ${it.status}. Response $body")
            }
            return HitronRouterWebSession(api, config)
        }
    }

    private fun CookieStore.preSession(): String? = cookies
        .filter { it.name == PRESESSION_COOKIE_NAME }
        .filterNot { it.isExpired(Date.from(Instant.now())) }
        .mapNotNull { it.value }
        .firstOrNull()

    private fun HitronRouterWebApi.login(preSession: String? = null) =
        login(config.username(), config.password(), preSession = preSession)
}

class HitronRouterWebSession(private val api: HitronRouterWebApi, private val config: HitronRouterConfig) : HitronRouterSession {

    override fun logout() {
        api.logout().close()
    }

    override fun systemInformation() = api.systemInformation().first()

    @WithSpan
    override fun ping(@SpanAttribute(value = "ipAddress") ipAddress: String) = diagnosticTest(DiagnosticTest(testmode = TestType.PING.testMode, testipaddr = ipAddress)).parsePingTestResult()

    @WithSpan
    override fun traceroute(@SpanAttribute(value = "ipAddress") ipAddress: String) = diagnosticTest(DiagnosticTest(testmode = TestType.TRACEROUTE.testMode, testipaddr = ipAddress)).parseTracerouteTestResult()

    private fun diagnosticTest(data: DiagnosticTest): DiagnosticTestStatus {
        data.start()
        Log.debug("Starting polling for diagnostic test result")
        var testStatus: DiagnosticTestStatus
        do {
            data.waitWithCancellation()
            testStatus = data.status()
            Log.debugv("Test status {0}", testStatus)
        } while (testStatus.testStatus == 0)
        return testStatus
    }

    private fun DiagnosticTest.start() = api.diagnosticTest(this).use {
        Log.debugv("Started test in {0}. Response {1}", it.status, it.bodyAsString())
        if (it.status != HttpStatus.SC_OK) {
            throw HitronRouterException("Failed to start diagnostic test. Status ${it.status}. Response ${it.bodyAsString()}")
        }
    }

    private fun DiagnosticTest.status() = api.diagnosticTestStatus().first()

    private fun DiagnosticTest.waitWithCancellation() {
        try {
            TimeUnit.SECONDS.sleep(1)
        } catch (ex: InterruptedException) {
            cancel()
            throw ex
        }
    }

    private fun DiagnosticTest.cancel() {
        Log.info("Cancelling diagnostic test")
        api.diagnosticTest(
            DiagnosticTest(
                testtype = testtype,
                testmode = -1,
                testipaddr = testipaddr,
            ),
        ).close()
    }

    override fun virginMediaConnectionStatus() = VirginMediaConnectionStatus(
        api.virginMediaBusinessAccountInformation().first(),
        api.virginTunnelPublicInformation().first(),
        api.virginTunnelServiceInformation().first(),
        api.virginTunnelRetries().first(),
    )

    override fun reboot() {
        if (config.dryRun()) {
            Log.info("Dry Run: Triggering reboot")
            return
        }
        api.reboot().use {
            Log.infov("Rebooted device {0}. Response {1}", it.status, it.bodyAsString())
            if (it.status != HttpStatus.SC_OK) {
                throw HitronRouterException("Failed to reboot device. Status ${it.status}. Response ${it.bodyAsString()}")
            }
        }
    }
}

private fun Response.bodyAsString() = readEntity(String::class.java)

open class HitronRouterException : Exception {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
}

open class ConcurrentSessionException : HitronRouterException {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
}

@JvmRecord
data class VirginMediaConnectionStatus(
    val accountInformation: VirginMediaBusinessAccountInformation,
    val tunnelPublicInformation: VirginTunnelPublicInformation,
    val tunnelServiceInformation: VirginTunnelServiceInformation,
    val tunnelRetries: VirginTunnelRetries,
)
