package uk.co.borismorris.hitron.web

import io.quarkus.logging.Log
import uk.co.borismorris.hitron.web.TestType.PING
import uk.co.borismorris.hitron.web.TestType.TRACEROUTE
import java.math.BigDecimal
import java.net.InetAddress
import java.time.Duration
import kotlin.text.RegexOption.COMMENTS
import kotlin.text.RegexOption.DOT_MATCHES_ALL
import kotlin.text.RegexOption.MULTILINE

private val regexOptions = setOf(MULTILINE, COMMENTS, DOT_MATCHES_ALL)
private val headerRowPattern = """
    PING\s++
    (?<destination>[\d.]++)\s++
    \((?<destinationHost>[^)]++)\)\s++
    from\s++
    (?<source>[\d.]++):\s++
    (?<bytes>\d++)\s++
    data\s++
    bytes
""".trimIndent().toRegex(regexOptions)
private val pingRowPattern = """
    (?<bytes>\d++)\s++
    bytes\s++
    from\s++
    (?<destination>[\d.]++):\s++
    seq=(?<seq>\d++)\s++
    ttl=(?<ttl>\d++)\s++
    time=(?<time>[\d.]++)\s++
    ms
""".trimIndent().toRegex(regexOptions)
private val packerTransmissionRowPattern = """
    (?<sent>\d++)\s++
    packets\s++
    transmitted,\s++
    (?<received>\d++)\s++
    packets\s++
    received,\s++
    (?<loss>[\d.]++)%\s++
    packet\s++
    loss
""".trimIndent().toRegex(regexOptions)
private val roundTripRowPattern = """
    round-trip\s++
    min/avg/max\s++
    =\s++
    (?<min>[\d.]++)/(?<avg>[\d.]++)/(?<max>[\d.]++)\s++
    ms
""".trimIndent().toRegex(regexOptions)
private val NANOS_IN_MILLI = BigDecimal.valueOf(1_000_000)

sealed class DiagnosticTestResult {
    abstract val status: DiagnosticTestStatus

    val testType: String
        get() = status.testType
    val testStatus: Int
        get() = status.testStatus
}

fun DiagnosticTestStatus.parseTestResult(type: TestType) = parseTestResult(type.testMode)

fun DiagnosticTestStatus.parseTestResult(mode: Int): DiagnosticTestResult = when (mode) {
    TRACEROUTE.testMode -> parseTracerouteTestResult()
    PING.testMode -> parsePingTestResult()
    else -> throw HitronRouterException("Cannot parse test mode $mode")
}

@Suppress("TooGenericExceptionCaught")
fun DiagnosticTestStatus.parsePingTestResult() = try {
    val lines = testResult.splitToSequence("#").iterator()
    val headerRow = lines.next().parseHeaderRow()
    val pings = buildList {
        var row = lines.next()
        while (row.isNotBlank()) {
            add(row.parsePingRow())
            row = lines.next()
        }
    }
    // Skip the statistics headers row
    lines.next()
    val packetStatistics = lines.next().parsePacketStatisticsRow()
    val roundTripStatistics = lines.next().parseRoundTripStatisticsRow()

    PingTestResult(this, headerRow, pings, packetStatistics, roundTripStatistics)
} catch (e: Exception) {
    throw HitronRouterException("Failed to parse ping test $this to PingTestResult", e)
}

fun DiagnosticTestStatus.parseTracerouteTestResult(): TracerouteTestResult {
    Log.warn("Trace route parsing not yet supported")
    return TracerouteTestResult(this)
}

fun String.parseHeaderRow(): HeaderRow {
    val match = headerRowPattern.matchEntire(this) ?: throw HitronRouterException("Failed to parse '$this' to packet statistics row")
    val groups = match.groups
    return HeaderRow(
        groups.getValue("destination").toInetAddress(),
        groups.getValue("source").toInetAddress(),
        groups.getValue("bytes").toInt(),
    )
}

fun String.parsePingRow(): Ping {
    val match = pingRowPattern.matchEntire(this) ?: throw HitronRouterException("Failed to parse '$this' to ping row")
    val groups = match.groups
    return Ping(
        groups.getValue("bytes").toInt(),
        groups.getValue("destination").toInetAddress(),
        groups.getValue("seq").toInt(),
        groups.getValue("ttl").toInt(),
        groups.getValue("time").toDuration(),
    )
}

fun String.parsePacketStatisticsRow(): PacketStatistics {
    val match = packerTransmissionRowPattern.matchEntire(this) ?: throw HitronRouterException("Failed to parse '$this' to packet statistics row")
    val groups = match.groups
    return PacketStatistics(
        groups.getValue("sent").toInt(),
        groups.getValue("received").toInt(),
        groups.getValue("loss").toBigDecimal(),
    )
}

fun String.parseRoundTripStatisticsRow() = roundTripRowPattern.matchEntire(this)?.let { match ->
    val groups = match.groups
    RoundTripStatistics(
        groups.getValue("min").toDuration(),
        groups.getValue("avg").toDuration(),
        groups.getValue("max").toDuration(),
    )
}

private fun String.toInetAddress() = InetAddress.getByName(this)

private fun String.toDuration() = Duration.ofNanos(toBigDecimal().multiply(NANOS_IN_MILLI).toLong())

private fun MatchGroupCollection.getValue(name: String) = get(name)?.value ?: throw NoSuchElementException("No key $name in $this")

data class PingTestResult(
    override val status: DiagnosticTestStatus,
    val headerRow: HeaderRow,
    val pings: List<Ping>,
    val packetStatistics: PacketStatistics,
    val roundTripStatistics: RoundTripStatistics?,
) : DiagnosticTestResult()

data class TracerouteTestResult(override val status: DiagnosticTestStatus) : DiagnosticTestResult()

@JvmRecord
data class HeaderRow(
    val destination: InetAddress,
    val source: InetAddress,
    val packetSize: Int,
)

@JvmRecord
data class Ping(
    val bytes: Int,
    val destination: InetAddress,
    val seq: Int,
    val ttl: Int,
    val duration: Duration,
)

@JvmRecord
data class PacketStatistics(
    val packetsTransmitted: Int,
    val packetsReceived: Int,
    val packetLoss: BigDecimal,
)

@JvmRecord
data class RoundTripStatistics(
    val min: Duration,
    val average: Duration,
    val max: Duration,
)
