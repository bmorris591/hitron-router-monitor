package uk.co.borismorris.hitron.web

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import io.quarkus.runtime.annotations.RegisterForReflection
import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.FormParam
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.QueryParam
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import java.time.Instant
import java.time.LocalDateTime

const val PRESESSION_COOKIE_NAME = "preSession"

@ApplicationScoped
@RegisterRestClient(configKey = "hitron-router-api")
interface HitronRouterWebApi {

    @POST
    @Path("/goform/login")
    @Consumes(value = [MediaType.APPLICATION_FORM_URLENCODED])
    @Produces(value = [MediaType.TEXT_HTML])
    fun login(
        @FormParam("usr") username: String,
        @FormParam("pwd") password: String,
        @FormParam("forcelogoff") forcelogoff: Int = 0,
        @FormParam("preSession") preSession: String? = null,
    ): Response

    @POST
    @Path("/goform/logout")
    @Consumes(value = [MediaType.APPLICATION_FORM_URLENCODED])
    @Produces(value = [MediaType.TEXT_HTML])
    fun logout(@FormParam("data") data: String = "byebye"): Response

    @GET
    @Path("/data/getSysInfo.asp")
    @Produces(value = [MediaType.APPLICATION_JSON])
    fun systemInformation(@QueryParam("_") time: Long = Instant.now().epochSecond): List<SystemInformation>

    @GET
    @Path("/data/getCsrf.asp")
    @Produces(value = [MediaType.APPLICATION_JSON])
    fun csrfToken(@QueryParam("_") time: Long = Instant.now().epochSecond): CsrfToken

    @POST
    @Path("/goform/TestDiag")
    @Consumes(value = [MediaType.APPLICATION_FORM_URLENCODED])
    @Produces(value = [MediaType.APPLICATION_JSON])
    @CsrfProtected
    fun diagnosticTest(@FormParam("model") data: DiagnosticTest): Response

    @GET
    @Path("/data/getAdminDiag.asp")
    @Produces(value = [MediaType.APPLICATION_JSON])
    fun diagnosticTestStatus(@QueryParam("_") time: Long = Instant.now().epochSecond): List<DiagnosticTestStatus>

    @GET
    @Path("/data/getVMBAccountInfo.asp")
    @Produces(value = [MediaType.APPLICATION_JSON])
    fun virginMediaBusinessAccountInformation(@QueryParam("_") time: Long = Instant.now().epochSecond): List<VirginMediaBusinessAccountInformation>

    @GET
    @Path("/data/vmb_public_info.asp")
    @Produces(value = [MediaType.APPLICATION_JSON])
    @CsrfProtected
    fun virginTunnelPublicInformation(@QueryParam("_") time: Long = Instant.now().epochSecond): List<VirginTunnelPublicInformation>

    @GET
    @Path("/data/vmb_service_info.asp")
    @Produces(value = [MediaType.APPLICATION_JSON])
    @CsrfProtected
    fun virginTunnelServiceInformation(@QueryParam("_") time: Long = Instant.now().epochSecond): List<VirginTunnelServiceInformation>

    @GET
    @Path("/data/vmb_retry_times.asp")
    @Produces(value = [MediaType.APPLICATION_JSON])
    @CsrfProtected
    fun virginTunnelRetries(@QueryParam("_") time: Long = Instant.now().epochSecond): List<VirginTunnelRetries>

    @POST
    @Path("/goform/Reboot")
    @Consumes(value = [MediaType.APPLICATION_FORM_URLENCODED])
    @Produces(value = [MediaType.APPLICATION_JSON])
    @CsrfProtected
    fun reboot(@FormParam("model") data: RebootModel = RebootModel(1)): Response
}

@RegisterForReflection
@JsonIgnoreProperties(ignoreUnknown = true)
@JvmRecord
data class SystemInformation(
    @JsonProperty("hwVersion") val hardwareVersion: String,
    @JsonProperty("swVersion") val softwareVersion: String,
    @JsonProperty("serialNumber") val serialNumber: String,
    @JsonProperty("rfMac") val hardwareAddress: String,
    @JsonProperty("wanIp") val wanIp: String,
    @JsonProperty("aftrName") val aftrName: String,
    @JsonProperty("aftrAddr") val aftrAddr: String,
    @JsonProperty("delegatedPrefix") val delegatedPrefix: String,
    @JsonProperty("lanIPv6Addr") val lanIPv6Addr: String,
    @JsonProperty("systemUptime") val systemUptime: String,
    @JsonProperty("systemTime")
    @JsonFormat(
        shape = STRING,
        pattern = "EEE MMM dd, yyyy, HH:mm:ss",
    )
    val systemTime: LocalDateTime,
    @JsonProperty("WRecPkt") val wanReceivedPackets: String,
    @JsonProperty("WSendPkt") val wanSentPackets: String,
    @JsonProperty("lanIp") val lanIp: String,
    @JsonProperty("LRecPkt") val lanReceivedPackets: String,
    @JsonProperty("LSendPkt") val lanSentPackets: String,
)

@RegisterForReflection
@JsonIgnoreProperties(ignoreUnknown = true)
@JvmRecord
data class CsrfToken(@JsonProperty("Csrf_token") val token: String)

@JsonEncoded
@RegisterForReflection
@JvmRecord
data class DiagnosticTest(
    @JsonProperty("testtype") val testtype: String = "IPv4",
    @JsonProperty("testflag") val testflag: Int = 1,
    @JsonProperty("testmode") val testmode: Int,
    @JsonProperty("testurl") val testurl: String = "",
    @JsonProperty("testipaddr") val testipaddr: String,
)

@JsonEncoded
@RegisterForReflection
@JvmRecord
data class RebootModel(@JsonProperty("reboot") val reboot: Int)

@RegisterForReflection
@JvmRecord
data class DiagnosticTestStatus(
    @JsonProperty("testtype") val testType: String,
    @JsonProperty("testresult") val testResult: String,
    @JsonProperty("teststatus") val testStatus: Int,
)

@RegisterForReflection
@JvmRecord
data class VirginMediaBusinessAccountInformation(
    @JsonProperty("Username") val username: String,
    @JsonProperty("UsernameSizeMin") val usernameSizeMin: String,
    @JsonProperty("UsernameSizeMax") val usernameSizeMax: String,
    @JsonProperty("Password") val password: String,
    @JsonProperty("PasswordSizeMin") val passwordSizeMin: String,
    @JsonProperty("PasswordSizeMax") val passwordSizeMax: String,
)

@RegisterForReflection
@JvmRecord
data class VirginTunnelPublicInformation(
    @JsonProperty("GetwayIp") val gatewayIp: String,
    @JsonProperty("StartIp") val startIp: String,
    @JsonProperty("EndIp") val endIp: String,
    @JsonProperty("Status") val status: String,
    @JsonProperty("Prefix") val cidrPrefix: String,
)

@RegisterForReflection
@JvmRecord
data class VirginTunnelServiceInformation(
    @JsonProperty("WanIp") val wanIp: String,
    @JsonProperty("Radius") val radius: String,
    @JsonProperty("GRETunnel") val greTunnelStatus: String,
)

@RegisterForReflection
@JvmRecord
data class VirginTunnelRetries(
    @JsonProperty("RetryTimes") val retryTimes: Int,
)
