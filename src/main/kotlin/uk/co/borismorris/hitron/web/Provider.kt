package uk.co.borismorris.hitron.web

import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.arc.Unremovable
import jakarta.enterprise.context.Dependent
import jakarta.enterprise.inject.spi.CDI
import jakarta.inject.Inject
import jakarta.ws.rs.core.Form
import jakarta.ws.rs.ext.ParamConverter
import jakarta.ws.rs.ext.ParamConverterProvider
import jakarta.ws.rs.ext.Provider
import jakarta.ws.rs.ext.WriterInterceptor
import jakarta.ws.rs.ext.WriterInterceptorContext
import org.eclipse.microprofile.rest.client.inject.RestClient
import java.lang.reflect.Method
import java.lang.reflect.Type

@Provider
class JsonEncodingParameterConverterProvider : ParamConverterProvider {

    val mapper: ObjectMapper = CDI.current().select(ObjectMapper::class.java).get()

    override fun <T : Any?> getConverter(
        rawType: Class<T>,
        genericType: Type?,
        annotations: Array<out Annotation>?,
    ): ParamConverter<T>? {
        if (rawType.isAnnotationPresent(JsonEncoded::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return JsonEncodingParameterConverter(rawType, mapper)
        }
        return null
    }
}

private const val INVOKED_METHOD = "org.eclipse.microprofile.rest.client.invokedMethod"

@Unremovable
@Dependent
@Provider
class CsrfInterceptor : WriterInterceptor {

    @setparam:RestClient
    @set:Inject
    lateinit var hitronApi: HitronRouterWebApi

    override fun aroundWriteTo(context: WriterInterceptorContext) {
        val invokedMethod = context.getProperty(INVOKED_METHOD) as Method?
        if (invokedMethod != null) {
            checkCsrf(invokedMethod, context)
        }
        context.proceed()
    }

    private fun checkCsrf(invokedMethod: Method, context: WriterInterceptorContext) {
        if (!invokedMethod.isAnnotationPresent(CsrfProtected::class.java)) {
            return
        }
        val csrfToken = hitronApi.csrfToken()
        (context.entity as Form)
            .param("CsrfToken", csrfToken.token)
            .param("CsrfTokenFlag", "1")
    }
}

class JsonEncodingParameterConverter<T>(private val type: Class<T>, private val mapper: ObjectMapper) : ParamConverter<T> {

    override fun toString(value: T?) = value?.let { mapper.writeValueAsString(it) }

    override fun fromString(value: String?) = value?.let { mapper.readValue(it, type) }
}

@Retention(AnnotationRetention.RUNTIME)
@Target(allowedTargets = [AnnotationTarget.CLASS])
annotation class JsonEncoded

@Retention(AnnotationRetention.RUNTIME)
@Target(allowedTargets = [AnnotationTarget.FUNCTION])
annotation class CsrfProtected
