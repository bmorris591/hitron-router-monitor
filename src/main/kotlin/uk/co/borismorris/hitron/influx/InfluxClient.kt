package uk.co.borismorris.hitron.influx

import com.influxdb.client.InfluxDBClientFactory
import com.influxdb.client.InfluxDBClientOptions
import com.influxdb.client.write.Point
import com.influxdb.client.write.WriteParameters
import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import uk.co.borismorris.hitron.conf.InfluxConfig

interface InfluxClient {
    fun writePoint(point: Point)
}

@ApplicationScoped
class JavaClientInfluxClient(private val influxConfig: InfluxConfig) : InfluxClient {
    private final val influxDBClient = InfluxDBClientOptions.builder()
        .url(influxConfig.url())
        .org(influxConfig.organization())
        .bucket(influxConfig.bucket())
        .also { influxConfig.authToken().ifPresent { token -> it.authenticateToken(token.toCharArray()) } }
        .build()
        .let { InfluxDBClientFactory.create(it) }

    val influxWrite = influxDBClient.writeApiBlocking

    @WithSpan
    override fun writePoint(point: Point) {
        influxWrite.writePoint(point, WriteParameters(influxConfig.precision(), null))
    }
}
