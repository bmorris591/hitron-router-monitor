import de.undercouch.gradle.tasks.download.Download
import io.gitlab.arturbosch.detekt.Detekt
import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile

buildscript {
    repositories {
        mavenCentral()
    }
}

plugins {
    java
    idea
    `project-report`
    alias(libs.plugins.quarkus)
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.allopen)
    alias(libs.plugins.versions)
    alias(libs.plugins.catalog.update)

    alias(libs.plugins.download)

    alias(libs.plugins.spotless)
    alias(libs.plugins.detekt)
}

group = "uk.co.borismorris.hitron"
version = "0.0.1-SNAPSHOT"

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_19
    targetCompatibility = JavaVersion.VERSION_19
}

tasks.withType<KotlinJvmCompile> {
    kotlinOptions {
        jvmTarget = "19"
        allWarningsAsErrors = true
        freeCompilerArgs = listOfNotNull(
            "-Xjsr305=strict",
            "-Xinline-classes",
            "-opt-in=kotlin.io.path.ExperimentalPathApi",
            "-opt-in=kotlin.ExperimentalStdlibApi",
        )
    }
}

allOpen {
    annotation("jakarta.enterprise.context.ApplicationScoped")
}

configurations.all {
    exclude(group = "commons-logging", module = "commons-logging")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    implementation(platform(libs.quarkus))
    implementation("io.quarkus:quarkus-core")
    implementation("io.quarkus:quarkus-config-yaml")
    implementation("io.quarkus:quarkus-arc")
    implementation("io.quarkus:quarkus-kotlin")
    implementation("io.quarkus:quarkus-jackson")
    implementation("io.quarkus:quarkus-scheduler")
    implementation("io.quarkus:quarkus-rest-client")
    implementation("io.quarkus:quarkus-rest-client-jackson")
    implementation("io.quarkus:quarkus-logging-json")
    implementation("io.micrometer:micrometer-core")
    implementation(libs.quarkus.micrometer.otlp)
    implementation("io.quarkus:quarkus-container-image-jib")
    implementation("io.quarkus:quarkus-opentelemetry")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation(libs.influx.client)

    runtimeOnly("org.jboss.slf4j:slf4j-jboss-logmanager")
    runtimeOnly("org.jboss.logging:commons-logging-jboss-logging")

    testImplementation("io.quarkus:quarkus-junit5")
    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation(libs.assertj)
    testImplementation("org.awaitility:awaitility")
    testImplementation(libs.wiremock)
    testImplementation("org.mockito:mockito-core")
    testImplementation("org.mockito:mockito-junit-jupiter")
    testImplementation(libs.mockito.kotlin)
    testImplementation(kotlin("test-junit5"))
}

spotless {
    kotlin {
        ktlint()
    }
    kotlinGradle {
        ktlint()
    }
}

val downloadDetektConfig by tasks.registering(Download::class) {
    src("https://gitlab.com/bmorris591/detekt/-/raw/main/detekt.yaml?inline=false")
    dest("$buildDir/detket.config")
    onlyIfModified(true)
    useETag("all")
}

detekt {
    buildUponDefaultConfig = true
    allRules = true
    config.setFrom(files(downloadDetektConfig.get().dest))
}

tasks["check"].dependsOn("detektMain")

tasks.withType<Detekt> {
    jvmTarget = "19"
    dependsOn(downloadDetektConfig)
}

tasks.withType<Test> {
    useJUnitPlatform()

    systemProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager")

    testLogging {
        exceptionFormat = FULL
        showStandardStreams = true
        events("skipped", "failed")
    }
}
